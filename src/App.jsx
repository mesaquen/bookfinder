import React, { Component } from 'react';
import { debounce } from 'throttle-debounce';
import DataTable from 'react-data-table-component';
import BookSource from './logic/BookSource';
import Searchbar from './component/searchbar/Searchbar';
import './App.css';
import DateRangePicker from './component/data-range-picker/DataRangePicker';
import EmptyState from './component/empty-state/EmptyState';
import ResultCounter from './component/result-counter/ResultCounter';
import DetailButton from './component/detail-button/DetailButton';
import Modal from './component/modal/Modal';
import BookDetails from './component/book-details/BookDetails';
/**
 * @author Mesaque do Nascimento Francisco <mesaquenf@gmail.com>
 * @since 2019-10-12
 */
export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      item: { dimensions: {} },
      startYear: null,
      endYear: null,
      generalSearch: null,
      data: [],
    };
  }

  componentDidMount() {
    this.fetchData();
  }

  componentDidUpdate(prevProps, prevState) {
    const { generalSearch, startYear, endYear } = this.state;
    const {
      generalSearch: prevSearch,
      startYear: prevStartYear,
      endYear: prevEndYear,
    } = prevState;

    if (
      (generalSearch !== null && generalSearch !== prevSearch) ||
      (startYear !== null && startYear !== prevStartYear) ||
      (endYear !== null && endYear !== prevEndYear)
    ) {
      this.debouncedFetchData();
    }
  }

  fetchData = async () => {
    const { generalSearch, startYear, endYear } = this.state;
    const data = await BookSource.list({ generalSearch, startYear, endYear });
    this.setState({ data });
  };

  debouncedFetchData = debounce(300, this.fetchData);

  handleSearchResults = data => this.setState(data);

  handleDetailClick = item => this.setState({ item, showModal: true });

  handleClose = () => this.setState({ showModal: false });

  handleChange = (id, value) =>
    this.setState({
      [id]: value,
    });

  onChange = {
    generalSearch: this.handleChange.bind(this, 'generalSearch'),
    startYear: this.handleChange.bind(this, 'startYear'),
    endYear: this.handleChange.bind(this, 'endYear'),
  };

  cellRenderer = row => {
    return (
      <div className="app__table-cell">
        <span className="app__book-title">{row.title}</span>
        <span className="app__isbn">({row.isbn})</span>
      </div>
    );
  };

  detailButtonRenderer = row => {
    return <DetailButton onClick={this.handleDetailClick} item={row} />;
  };

  columns = [
    {
      name: 'Livro',
      selector: 'title',
      sortable: true,
      cell: this.cellRenderer,
      grow: 2,
    },

    {
      name: 'Autor',
      selector: 'author',
      sortable: true,
      grow: 0,
    },

    {
      name: 'Editora',
      selector: 'publisher',
      sortable: true,
      grow: 1,
    },

    {
      name: 'Ano',
      selector: 'year',
      sortable: true,
      right: true,
      grow: 0,
    },

    {
      name: 'Ações',
      button: true,
      center: true,
      grow: 0,
      cell: this.detailButtonRenderer,
    },
  ];

  renderDataTable = data => {
    if (data && data.length) {
      return (
        <DataTable columns={this.columns} data={data} pagination noHeader />
      );
    }

    return <EmptyState />;
  };

  render() {
    const { data, item, showModal } = this.state;

    return (
      <>
        <div className="app__header">
          <h1 className="app__company-title">SUPERO</h1>
          <Searchbar
            onSearch={this.onChange.generalSearch}
            placeholder="Busque livros pelo título, autor ou ISBN"
            buttonLabel="Buscar"
          />
        </div>
        <hr />
        <div className="app__filter-container">
          <DateRangePicker
            onChangeStartYear={this.onChange.startYear}
            onChangeEndYear={this.onChange.endYear}
          />
          <ResultCounter count={data.length} />
        </div>
        <hr />
        <div className="app__table-container">{this.renderDataTable(data)}</div>
        <Modal open={showModal} onClose={this.handleClose} title={item.title}>
          <BookDetails
            title={item.title}
            isbn={item.isbn}
            author={item.author}
            year={item.year}
            language={item.language}
            publisher={item.publisher}
            weight={item.weight}
            width={item.dimensions.width}
            length={item.dimensions.length}
            height={item.dimensions.height}
          />
        </Modal>
      </>
    );
  }
}
