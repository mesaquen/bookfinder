import React, { memo } from 'react';
import PropTypes from 'prop-types';
import './BookDetails.style.css';
/**
 * @author Mesaque do Nascimento Francisco <mesaquenf@gmail.com>
 * @since 2019-10-12
 */
function BookDetails(props) {
  const {
    title,
    author,
    publisher,
    year,
    isbn,
    language,
    weight,
    width,
    length,
    height,
  } = props;
  return (
    <div className="book-details__container">
      <span>{`
        Título: ${title}
      `}</span>
      <span>{`ISBN: ${isbn}`}</span>
      <span>{`Autor: ${author}`}</span>
      <span>{`Editora: ${publisher}`}</span>
      <span>{`Ano: ${year}`}</span>
      <span>{`Idioma: ${language}`}</span>
      <span>{`Peso: ${weight}g`}</span>
      <span>{`Largura: ${width}cm`}</span>
      <span>{`Comprimento: ${length}cm`}</span>
      <span>{`Altura: ${height}cm`}</span>
    </div>
  );
}

BookDetails.propTypes = {
  title: PropTypes.string.isRequired,
  author: PropTypes.string.isRequired,
  publisher: PropTypes.string.isRequired,
  language: PropTypes.string.isRequired,
  year: PropTypes.number.isRequired,
  isbn: PropTypes.number.isRequired,
  weight: PropTypes.number.isRequired,
  width: PropTypes.number.isRequired,
  length: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
};

export default memo(BookDetails);
