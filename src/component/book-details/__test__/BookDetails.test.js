import React from 'react';
import renderer from 'react-test-renderer';
import BookDetails from '../BookDetails';
/**
 * @author Mesaque do Nascimento Francisco <mesaquenf@gmail.com>
 * @since 2019-10-12
 */
describe('Testing DateRangePicker component', () => {
  it('should have same snapshot', () => {
    const component = renderer.create(
      <BookDetails
        title="title"
        isbn={1234567894561}
        author="author"
        year={2019}
        language="pt-br"
        publisher="publisher"
        weight={320}
        width={20}
        length={10}
        height={2}
      />
    );

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
