import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import InputNumber from '../input-number/InputNumber';
import './DataRangePicker.style.css';
/**
 * @author Mesaque do Nascimento Francisco <mesaquenf@gmail.com>
 * @since 2019-10-12
 */
export default class DateRangePicker extends PureComponent {
  static propTypes = {
    startYear: PropTypes.number,
    endYear: PropTypes.number,
    onChangeStartYear: PropTypes.func.isRequired,
    onChangeEndYear: PropTypes.func.isRequired,
  };

  static defaultProps = {
    startYear: null,
    endYear: null,
  };

  handleChangeStartYear = value => {
    const { onChangeStartYear } = this.props;
    onChangeStartYear.call(null, value);
  };

  handleChangeEndYear = value => {
    const { onChangeEndYear } = this.props;
    onChangeEndYear.call(null, value);
  };

  render() {
    const { startYear, endYear } = this.props;
    return (
      <div>
        <span className="data-range-picker__title">
          Filtrar ano de publicação:
        </span>
        <InputNumber onChange={this.handleChangeStartYear} value={startYear} />
        <i className="data-range-picker__icon fa fa-calendar" />
        <span className="data-range-picker__junction">até</span>
        <InputNumber onChange={this.handleChangeEndYear} value={endYear} />
        <i className="data-range-picker__icon  fa fa-calendar" />
      </div>
    );
  }
}
