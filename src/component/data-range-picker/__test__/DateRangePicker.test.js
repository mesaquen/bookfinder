import React from 'react';
import renderer from 'react-test-renderer';
import DateRangePicker from '../DataRangePicker';
/**
 * @author Mesaque do Nascimento Francisco <mesaquenf@gmail.com>
 * @since 2019-10-12
 */
describe('Testing DateRangePicker component', () => {
  it('should have same snapshot', () => {
    const component = renderer.create(
      <DateRangePicker
        onChangeStartYear={jest.fn()}
        onChangeEndYear={jest.fn()}
      />
    );

    const tree = component.toJSON();

    expect(tree).toMatchSnapshot();
  });
});
