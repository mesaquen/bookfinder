import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import './DetailButton.style.css';
/**
 * @author Mesaque do Nascimento Francisco <mesaquenf@gmail.com>
 * @since 2019-10-12
 */
export default class DetailButton extends PureComponent {
  static propTypes = {
    onClick: PropTypes.func.isRequired,
    item: PropTypes.object.isRequired,
  };

  handleClick = () => {
    const { onClick, item } = this.props;

    onClick.call(null, item);
  };

  render() {
    return (
      <button
        className="detail-button__button"
        type="button"
        onClick={this.handleClick}
      >
        Detalhes
      </button>
    );
  }
}
