import React from 'react';
import renderer from 'react-test-renderer';
import DetailButton from '../DetailButton';
/**
 * @author Mesaque do Nascimento Francisco <mesaquenf@gmail.com>
 * @since 2019-10-12
 */
describe('Test DetailButton component', () => {
  it('Should match snapshot', () => {
    const callback = jest.fn();
    const component = renderer.create(
      <DetailButton onClick={callback} item={{}} />
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
