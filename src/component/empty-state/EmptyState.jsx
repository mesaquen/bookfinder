import React, { PureComponent } from 'react';
import './EmptyState.style.css';
/**
 * @author Mesaque do Nascimento Francisco <mesaquenf@gmail.com>
 * @since 2019-10-12
 */
export default class EmptyState extends PureComponent {
  render() {
    return (
      <div className="empty-state__container">
        <i className="fa fa-exclamation-circle empty-state__icon" />
        <span className="empty-state__message">
          Não há resultados a serem exibidos
        </span>
      </div>
    );
  }
}
