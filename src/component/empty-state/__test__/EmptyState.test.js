import React from 'react';
import renderer from 'react-test-renderer';
import EmptyState from '../EmptyState';
/**
 * @author Mesaque do Nascimento Francisco <mesaquenf@gmail.com>
 * @since 2019-10-12
 */
describe('Test EmtptyState component', () => {
  it('Should have same snapshot', () => {
    const component = renderer.create(<EmptyState />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
