import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import InputText from '../input-text/InputText';
/**
 * @author Mesaque do Nascimento Francisco <mesaquenf@gmail.com>
 * @since 2019-10-12
 */
export default class InputNumber extends PureComponent {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
  };

  handleChange = strValue => {
    const { onChange } = this.props;
    const value = parseFloat(strValue);
    onChange.call(null, value);
  };

  render() {
    return (
      <InputText {...this.props} onChange={this.handleChange} type="number" />
    );
  }
}
