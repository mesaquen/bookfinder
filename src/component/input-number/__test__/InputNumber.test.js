import React from 'react';
import renderer from 'react-test-renderer';
import InputNumber from '../InputNumber';
/**
 * @author Mesaque do Nascimento Francisco <mesaquenf@gmail.com>
 * @since 2019-10-12
 */
describe('Test InputNumber component', () => {
  it('Should have same snapshot', () => {
    const component = renderer.create(
      <InputNumber onChange={jest.fn()} value={123} />
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
