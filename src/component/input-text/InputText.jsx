import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
/**
 * @author Mesaque do Nascimento Francisco <mesaquenf@gmail.com>
 * @since 2019-10-12
 */
export default class InputText extends PureComponent {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
  };

  handleChange = e => {
    const { onChange } = this.props;
    e.preventDefault();
    onChange.call(null, e.currentTarget.value);
  };

  render() {
    return <input {...this.props} onChange={this.handleChange} />;
  }
}
