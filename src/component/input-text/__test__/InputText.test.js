import React from 'react';
import renderer from 'react-test-renderer';
import InputText from '../InputText';
/**
 * @author Mesaque do Nascimento Francisco <mesaquenf@gmail.com>
 * @since 2019-10-12
 */
describe('Test InputNumber component', () => {
  it('Should have same snapshot', () => {
    const component = renderer.create(
      <InputText onChange={jest.fn()} value="Sample" />
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
