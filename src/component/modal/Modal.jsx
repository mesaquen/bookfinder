import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import './Modal.style.css';
/**
 * @author Mesaque do Nascimento Francisco <mesaquenf@gmail.com>
 * @since 2019-10-12
 */
export default class Modal extends PureComponent {
  static propTypes = {
    title: PropTypes.string,
    onClose: PropTypes.func.isRequired,
    children: PropTypes.node,
    open: PropTypes.bool,
  };

  static defaultProps = {
    title: null,
    children: null,
    open: false,
  };

  handleClose = () => {
    const { onClose } = this.props;
    onClose.call(null);
  };

  renderTitle = title => {
    if (typeof title === 'string') {
      return <h1 className="modal__title">{title}</h1>;
    }
    return null;
  };

  render() {
    const { open, children, title } = this.props;
    const showHideClass = open
      ? 'modal__main modal__main--display-block'
      : 'modal__main modal__main--display-none';
    return (
      <div className={showHideClass}>
        <div className="modal__container">
          <div className="modal__close-icon" onClick={this.handleClose}>
            <i className="fa fa-close" />
          </div>
          {this.renderTitle(title)}
          <section className="modal__content">{children}</section>
        </div>
      </div>
    );
  }
}
