import React from 'react';
import renderer from 'react-test-renderer';
import Modal from '../Modal';
/**
 * @author Mesaque do Nascimento Francisco <mesaquenf@gmail.com>
 * @since 2019-10-12
 */
describe('Testing Modal component', () => {
  const callback = jest.fn();
  let component = renderer.create(
    <Modal title="modal title" onClose={callback}>
      <span>modal content</span>
    </Modal>
  );
  let tree = component.toJSON();
  it('should have same snapshot - closed', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should have same snapshot - open', () => {
    component = renderer.create(
      <Modal title="modal title" onClose={callback} open>
        <span>modal content</span>
      </Modal>
    );

    tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
