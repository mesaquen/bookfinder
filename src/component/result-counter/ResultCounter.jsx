import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
/**
 * @author Mesaque do Nascimento Francisco <mesaquenf@gmail.com>
 * @since 2019-10-12
 */
export default class ResultCounter extends PureComponent {
  static propTypes = {
    count: PropTypes.number.isRequired,
  };

  getMessage = count => {
    if (count > 0) {
      if (count > 1) {
        return `${count} resultados encontrados`;
      }
      return `${count} resultado encontrado`;
    }

    return 'Nenhum resultado encontrado';
  };

  render() {
    const { count } = this.props;
    return <span>{this.getMessage(count)}</span>;
  }
}
