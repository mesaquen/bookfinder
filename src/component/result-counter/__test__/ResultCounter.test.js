import React from 'react';
import renderer from 'react-test-renderer';
import ResultCounter from '../ResultCounter';
/**
 * @author Mesaque do Nascimento Francisco <mesaquenf@gmail.com>
 * @since 2019-10-12
 */
describe('Test InputNumber component', () => {
  let component = renderer.create(<ResultCounter count={0} />);
  let tree = component.toJSON();
  it('Should have same snapshot - with zero count', () => {
    expect(tree).toMatchSnapshot();
  });
  it('Should have same snapshot - with one count', () => {
    component = renderer.create(<ResultCounter count={1} />);
    tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
  it('Should have same snapshot - with many count', () => {
    component = renderer.create(<ResultCounter count={100} />);
    tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
