import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import './Searchbar.style.css';
import InputText from '../input-text/InputText';
/**
 * @author Mesaque do Nascimento Francisco <mesaquenf@gmail.com>
 * @since 2019-10-12
 */
export default class Searchbar extends PureComponent {
  static propTypes = {
    onSearch: PropTypes.func.isRequired,
    buttonLabel: PropTypes.string.isRequired,
    placeholder: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      value: null,
    };
  }

  handleChange = value => this.setState({ value });

  handleClick = () => {
    const { onSearch } = this.props;
    const { value } = this.state;

    onSearch.call(null, value);
  };

  render() {
    const { buttonLabel, placeholder } = this.props;
    return (
      <div className="searchbar__container">
        <div className="searchbar__input-container">
          <div className="searchbar__icon">
            <i className="fa fa-search" />
          </div>
          <InputText
            onChange={this.handleChange}
            className="searchbar__input"
            placeholder={placeholder}
          />
        </div>
        <button
          type="button"
          className="searchbar__button"
          onClick={this.handleClick}
        >
          {buttonLabel}
        </button>
      </div>
    );
  }
}
