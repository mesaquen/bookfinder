import React from 'react';
import renderer from 'react-test-renderer';
import Searchbar from '../Searchbar';
/**
 * @author Mesaque do Nascimento Francisco <mesaquenf@gmail.com>
 * @since 2019-10-12
 */
describe('Testing Searchbar component', () => {
  it('should have same snapshot', () => {
    const component = renderer.create(
      <Searchbar
        onSearch={jest.fn()}
        placeholder="placeholder title"
        buttonLabel="search button label"
      />
    );

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
