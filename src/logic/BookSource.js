import BookData from './BookData';
/**
 * @author Mesaque do Nascimento Francisco <mesaquenf@gmail.com>
 * @since 2019-10-12
 */
export default new (class BookSource {
  list(params = {}) {
    const { generalSearch, startYear, endYear } = params;
    let results = BookData;

    if (generalSearch) {
      results = results.filter(book => {
        return (
          book.title.toLowerCase().includes(generalSearch.toLowerCase()) ||
          book.author.toLowerCase().includes(generalSearch.toLowerCase()) ||
          book.isbn.toString().includes(generalSearch)
        );
      });
    }

    if (startYear) {
      results = results.filter(book => book.year >= startYear);
    }

    if (endYear) {
      results = results.filter(book => book.year <= endYear);
    }

    return results;
  }
})();
